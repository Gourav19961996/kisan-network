package com.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.models.SentSMS;
import com.kisannetworkcodingchallenge.R;

import java.util.List;

public class SmsAdapter extends RecyclerView.Adapter<SmsAdapter.viewholder> {

    private List<SentSMS> smsList;
    private Context context;
    public SmsAdapter(Context context, List<SentSMS> smsList) {

        this.context = context;
        this.smsList = smsList;

    }

    @NonNull
    @Override
    public SmsAdapter.viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.sent_message_container, parent, false);
        return new SmsAdapter.viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SmsAdapter.viewholder holder, int position) {

        SentSMS sentSMS = smsList.get(position);
        holder.tvFirstName.setText(sentSMS.getFirstName());
        holder.tvLastName.setText(sentSMS.getLastName());
        holder.tvOtp.setText(sentSMS.getOtp());
        holder.tvDateTime.setText(sentSMS.getDate());

    }

    @Override
    public int getItemCount() {
        return smsList.size();
    }

    public class viewholder extends RecyclerView.ViewHolder {

        public TextView tvFirstName, tvLastName, tvDateTime, tvOtp;

        public viewholder(@NonNull View itemView) {
            super(itemView);

            tvFirstName = itemView.findViewById(R.id.tvFirstName);
            tvLastName = itemView.findViewById(R.id.tvLastName);
            tvDateTime = itemView.findViewById(R.id.tvDateTime);
            tvOtp = itemView.findViewById(R.id.tvOtp);
        }
    }
}
