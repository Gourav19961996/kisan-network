package com.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.lisners.ContactClickLisner;
import com.models.ContactList;
import com.kisannetworkcodingchallenge.R;

import java.util.List;

public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.viewholder> {

    private Context context;
    private List<ContactList> contactList;

    public ContactAdapter(Context context, List<ContactList> contactList) {

        this.context = context;
        this.contactList = contactList;

    }

    @NonNull
    @Override
    public viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.contact_list_container, parent, false);
        return new ContactAdapter.viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull viewholder holder, int position) {
        ContactList contact = contactList.get(position);
        holder.tvFirstName.setText(contact.getFirstName());
        holder.tvLastName.setText(contact.getLastName());
        holder.tvEmail.setText(contact.getEmail());

        holder.ll_contact.setOnClickListener(new ContactClickLisner(context,contact.getFirstName(),contact.getLastName(), contact.getEmail(),contact.getPhone(),contact.getPhoto()));




    }

    @Override
    public int getItemCount() {
        return contactList.size();
    }

    public class viewholder extends RecyclerView.ViewHolder{

        public TextView tvFirstName, tvLastName, tvEmail;
        public LinearLayout ll_contact;

        public viewholder(@NonNull View itemView) {
            super(itemView);

            tvFirstName = itemView.findViewById(R.id.tvFirstName);
            tvLastName = itemView.findViewById(R.id.tvLastName);
            tvEmail = itemView.findViewById(R.id.tvEmail);
            ll_contact = itemView.findViewById(R.id.ll_contact);
        }
    }
}
