package com.lisners;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.activity.ContactInfoActivity;

public class ContactClickLisner implements View.OnClickListener {

    private Context context;
    private String firstName;
    private String lastName;
    private String email;
    private String phone;
    private String photo;

    public ContactClickLisner(Context context, String firstName, String lastName, String email, String phone, String photo) {

        this.context = context;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phone = phone;
        this.photo = photo;

    }

    @Override
    public void onClick(View view) {

        Intent intent = new Intent(context, ContactInfoActivity.class);
        intent.putExtra("firstName", firstName);
        intent.putExtra("lastName", lastName);
        intent.putExtra("email", email);
        intent.putExtra("phone", phone);
        intent.putExtra("photo", photo);
        context.startActivity(intent);

    }
}
