package com.classes;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

class PrefSingleton {

private static PrefSingleton mInstance;
private Context mContext;
//
private SharedPreferences mMyPreferences;

private PrefSingleton(){ }

public static PrefSingleton getInstance(){
        if (mInstance == null) mInstance = new PrefSingleton();
        return mInstance;
        }

public void Initialize(Context ctxt){
        mContext = ctxt;
        //
        mMyPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        }

public SharedPreferences getmMyPreferences() {
        return mMyPreferences;
        }
        }