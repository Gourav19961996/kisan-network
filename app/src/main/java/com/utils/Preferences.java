package com.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.constants.Constants;

import static android.content.Context.MODE_PRIVATE;

public class Preferences {
    private static Preferences preferences;
    private Context context;

    public Preferences(Context context) {
        this.context = context;
    }

    public SharedPreferences.Editor getEditor() {
        if (preferences == null)
            init();

        return context.getSharedPreferences(Constants.DB_NAME, MODE_PRIVATE).edit();
    }

    public SharedPreferences getPreferences() {
        if (preferences == null)
            init();

        return context.getSharedPreferences(Constants.DB_NAME, MODE_PRIVATE);
    }

    public static void put(String key, String value) {
        init();
        preferences.getEditor().putString(key, value).apply();

    }

    public static void put(String key, int value) {
        init();
        preferences.getEditor().putInt(key, value).apply();

    }

    public static void put(String key, float value) {
        init();
        preferences.getEditor().putFloat(key, value).apply();

    }

    public static void put(String key, long value) {
        init();
        preferences.getEditor().putLong(key, value).apply();

    }

    public static void put(String key, boolean value) {
        init();
        preferences.getEditor().putBoolean(key, value).apply();

    }

    public static String getString(String key, String defaultValue) {
        init();
        return preferences.getPreferences().getString(key, defaultValue);
    }

    public static float getFloat(String key, float defaultValue) {
        init();
        return preferences.getPreferences().getFloat(key, defaultValue);
    }

    public static int getInt(String key, int defaultValue) {
        init();
        return preferences.getPreferences().getInt(key, defaultValue);
    }

    public static long getLong(String key, long defaultValue) {
        init();
        return preferences.getPreferences().getLong(key, defaultValue);
    }

    public static boolean getBoolean(String key, boolean defaultValue) {
        init();
        return preferences.getPreferences().getBoolean(key, defaultValue);
    }


    public static Preferences getInstance() {
        if (preferences != null)
            return preferences;
        else
            preferences = new Preferences(BaseApplication.getContext());
        return null;
    }

    public static void init() {
        preferences = new Preferences(BaseApplication.getContext());
    }

}