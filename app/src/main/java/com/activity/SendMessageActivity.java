package com.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.constants.Constants;
import com.models.SentSMS;
import com.google.gson.Gson;
import com.kisannetworkcodingchallenge.R;
import com.kisannetworkcodingchallenge.databinding.ActivitySendMessageBinding;
import com.utils.Preferences;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

public class SendMessageActivity extends AppCompatActivity {

    private static final String TAG = SendMessageActivity.class.getCanonicalName();
    public ActivitySendMessageBinding binding;
    public static final String ACCOUNT_SID = System.getenv("ACa15ef3497099a2241c546ebe2bcb85d9");
    public static final String AUTH_TOKEN = System.getenv("46122809353271aaa3f593489b54346d");
    private String phone, firstName, lastName, randomNumber;
    private List<SentSMS> smsList;
    private JSONArray dataArray = null;
    private Date currentTime = Calendar.getInstance().getTime();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_send_message);


        String sentSmsString = Preferences.getString(Constants.SEND_SMS, "");
        if (sentSmsString.equals(""))
            smsList = new ArrayList<>();
        else {
            try {
                dataArray = new JSONArray(sentSmsString);
                smsList = new ArrayList<>();
                for (int i1 = 0; i1 < dataArray.length(); i1++) {
                    SentSMS sentSMS = new Gson().fromJson(String.valueOf(dataArray.get(i1)), SentSMS.class);
                    smsList.add(sentSMS);
                }
                Log.d(TAG, "onCreate: ");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().permitAll().build());
        }


        Intent i = getIntent();
        firstName = i.getStringExtra("firstName");
        lastName = i.getStringExtra("lastName");
        phone = i.getStringExtra("phone");

        randomNumber = getRandomNumberString();

        binding.tvFirstName.setText(firstName);
        binding.tvLastName.setText(lastName);
        binding.tvOTP.setText(randomNumber);

        binding.llSendSMS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendSMS(phone, randomNumber);
                binding.llSendSMS.setClickable(false);
            }
        });
    }

    private void sendSMS(String toPhoneNumber, String message) {
        binding.pbTabProgress.setVisibility(View.VISIBLE);
        String base64EncodedCredentials = "Basic " + Base64.encodeToString(("ACa15ef3497099a2241c546ebe2bcb85d9" + ":" + "46122809353271aaa3f593489b54346d").getBytes(), Base64.NO_WRAP);
        AndroidNetworking.post("https://api.twilio.com/2010-04-01/Accounts/ACa15ef3497099a2241c546ebe2bcb85d9/SMS/Messages")
                .addBodyParameter("From", "+16265873616")
                .addBodyParameter("To", toPhoneNumber)
                .addBodyParameter("Body", "Hi " + firstName + " " + lastName + "." + "Your OTP is: " + message)
                .addHeaders("Authorization", base64EncodedCredentials)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        binding.pbTabProgress.setVisibility(View.GONE);
                        Intent intent = new Intent(SendMessageActivity.this, MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        Toast.makeText(SendMessageActivity.this, "SMS Sent!!", Toast.LENGTH_LONG).show();
                        binding.llSendSMS.setClickable(true);
                        SentSMS sentSMS = new SentSMS();
                        sentSMS.setFirstName(firstName);
                        sentSMS.setLastName(lastName);
                        sentSMS.setOtp(message);
                        sentSMS.setDate(String.valueOf(currentTime));
                        smsList.add(sentSMS);
                        saveToPref(smsList);

                    }

                    @Override
                    public void onError(ANError error) {
                        binding.pbTabProgress.setVisibility(View.GONE);
                        if (error.getErrorCode() == 400)
                            Toast.makeText(SendMessageActivity.this, "The number is unverified. Trial accounts cannot send messages to unverified numbers. <Status>400</Status>", Toast.LENGTH_LONG).show();
                        else if (error.getErrorCode() == 500)
                            Toast.makeText(SendMessageActivity.this, "SERVER ERROR", Toast.LENGTH_LONG).show();
                        else if (error.getErrorCode() == 503)
                            Toast.makeText(SendMessageActivity.this, "SERVICE UNAVAILABLE", Toast.LENGTH_LONG).show();
                        else if (error.getErrorCode() == 429)
                            Toast.makeText(SendMessageActivity.this, "TOO MANY REQUESTS", Toast.LENGTH_LONG).show();
                        else
                            Toast.makeText(SendMessageActivity.this, "Error. Please try again!!", Toast.LENGTH_LONG).show();
                        binding.llSendSMS.setClickable(true);
                    }
                });
    }

    private void saveToPref(List<SentSMS> sentSmeList) {
        String sentSms = new Gson().toJson(sentSmeList);
        Preferences.put(Constants.SEND_SMS, sentSms);

    }

    public static String getRandomNumberString() {
        // It will generate 6 digit random Number.
        // from 0 to 999999
        Random rnd = new Random();
        int number = rnd.nextInt(999999);

        // this will convert any number sequence into 6 character.
        return String.format("%06d", number);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        // Checks the orientation of the screen
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            Toast.makeText(this, "landscape", Toast.LENGTH_SHORT).show();
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            Toast.makeText(this, "portrait", Toast.LENGTH_SHORT).show();
        }
    }

}