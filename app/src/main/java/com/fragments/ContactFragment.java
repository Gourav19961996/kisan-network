package com.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.adapters.ContactAdapter;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.kisannetworkcodingchallenge.R;
import com.models.ContactList;

import org.json.JSONArray;

import java.util.List;

import static android.content.ContentValues.TAG;


public class ContactFragment extends Fragment {

    public RecyclerView rv_contactList;
    private ProgressBar pbTabProgressBar;


    public ContactFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle saveinstancestate) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(com.kisannetworkcodingchallenge.R.layout.contact_fragment, container, false);

        rv_contactList = rootView.findViewById(R.id.rv_contactList);
        pbTabProgressBar = rootView.findViewById(R.id.pbTabProgressBar);

        getContactListData();

        return rootView;

    }

    private void getContactListData() {
        AndroidNetworking.get("https://hangovercure-e8092.firebaseio.com/kisan_network/get_contacts.json")
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        List<ContactList> contactList = ContactList.fromJson(response);
                        // do anything with response

                        rv_contactList.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                        ContactAdapter contactAdapter = new ContactAdapter(getContext(), contactList);
                        rv_contactList.setAdapter(contactAdapter);
                        pbTabProgressBar.setVisibility(View.GONE);

                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.d(TAG, "onError: " + anError);
                        Toast.makeText(getContext(), "Networking Error", Toast.LENGTH_SHORT).show();
                        // handle error
                    }
                });
    }
}
