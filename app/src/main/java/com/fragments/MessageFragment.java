package com.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.adapters.SmsAdapter;
import com.constants.Constants;
import com.google.gson.Gson;
import com.kisannetworkcodingchallenge.R;
import com.models.SentSMS;
import com.utils.Preferences;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MessageFragment extends Fragment {

    private static final String TAG = MessageFragment.class.getCanonicalName();
    public RecyclerView rvSentMessages;
    private ProgressBar pbTabProgressBar;
    private List<SentSMS> smsList;
    private JSONArray dataArray = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle saveinstancestate) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.message_fragment, container, false);

        rvSentMessages = rootView.findViewById(R.id.rvSentMessages);
        pbTabProgressBar = rootView.findViewById(R.id.pbTabProgressBar);

        String sentSmsString = Preferences.getString(Constants.SEND_SMS, "");
        if (sentSmsString.equals(""))
            smsList = new ArrayList<>();
        else {
            try {
                dataArray = new JSONArray(sentSmsString);
                smsList = new ArrayList<>();
                for (int i1 = 0; i1 < dataArray.length(); i1++) {
                    SentSMS sentSMS = new Gson().fromJson(String.valueOf(dataArray.get(i1)), SentSMS.class);
                    smsList.add(sentSMS);
                }
                Log.d(TAG, "onCreate: ");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        initRecyclerView(smsList);

        return rootView;

    }

    private void initRecyclerView(List<SentSMS> smsList) {
        Collections.reverse(smsList);
        rvSentMessages.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        SmsAdapter smsAdapter = new SmsAdapter(getContext(), smsList);
        rvSentMessages.setAdapter(smsAdapter);
        pbTabProgressBar.setVisibility(View.GONE);

    }
}